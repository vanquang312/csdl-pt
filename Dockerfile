FROM node

WORKDIR /var/www/html

COPY . /var/www/html

RUN npm i
ENV APP_BASENAME_PATH=''
ENV PORT=4200

CMD [ "npm", "start" ]