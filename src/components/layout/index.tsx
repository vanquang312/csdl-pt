import React, { useCallback } from "react"
import { Layout, Menu, Breadcrumb } from "antd"
import { Link } from "react-router-dom"
import "./styles.css"

const { Header, Content, Footer } = Layout

const LayoutComponents = ({ children }) => {
  return (
    <Layout className="layout" style={{ height: "100vh" }}>
      <Header>
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]}>
          <Menu.Item key="1">
            <Link to={"/"}>Tiếp nhận</Link>
          </Menu.Item>
          <Menu.Item key="2">
            <Link to={"/kham-benh"}>Khám bệnh</Link>
          </Menu.Item>
          <Menu.Item key="3">
            <Link to={"/"}>Cận lâm sàng</Link>
          </Menu.Item>
        </Menu>
      </Header>
      <Content style={{ padding: "0 50px" }}>
        <div className="site-layout-content">{children}</div>
      </Content>
    </Layout>
  )
}

export default LayoutComponents
