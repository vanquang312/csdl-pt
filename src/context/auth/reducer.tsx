import { IAuth, TAuthAction } from "@common/index"
// import { uniq } from "lodash-es";

export type FCAuthReducer<IAuth, TAuthAction> = (
  prevState: IAuth,
  action: TAuthAction
) => IAuth

export const authReducer: FCAuthReducer<IAuth, TAuthAction> = (
  prevState: IAuth,
  action: TAuthAction
): IAuth => {
  switch (action.type) {
    case "SET_AUTH":
      return { ...prevState, isAuth: action.payload.isAuth }
    case "SET_USER":
      return { ...prevState, user: action.payload.user }
    case "SET_AUTHEN":
      const state = { ...prevState }
      if (action.payload.isAuth) {
        state.isAuth = true
        state.user = action.payload.user
        // state.user.permissions = uniq(state.user.permissions);
      } else {
        state.isAuth = false
        state.user = null
      }
      return state
    default:
      return { ...prevState }
  }
}
