import { IAuth } from "@common/index"

export const initAuthState: IAuth = {
  isAuth: false,
  user: null
}

export * from "./reducer"
