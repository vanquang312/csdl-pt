import { AuthContextProvider } from "@context/index"
import React from "react"
import AppRouters from "./app"

const App = () => {
  return (
    <AuthContextProvider>
      <AppRouters />
    </AuthContextProvider>
  )
}

export default App
