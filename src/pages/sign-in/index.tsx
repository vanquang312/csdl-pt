import React from "react"
import { Link } from "react-router-dom"
import "./style.css"

const Home = () => {
  return (
    <div className="sign-in-container">
      <p>SignIn</p>
      <Link to="/">Home</Link>
      <Link to="/sign-up">SignUp</Link>
    </div>
  )
}

export default Home
