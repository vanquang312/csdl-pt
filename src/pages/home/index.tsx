import { GridComponent } from "@components/grid"
import { useAuth } from "@utils/index"
import React, { useCallback } from "react"
import { Link } from "react-router-dom"
// import styles from "./index.module.css";

const Home = () => {
  const { isAuth, user, dispatchAuth } = useAuth()

  const login = useCallback(() => {
    dispatchAuth({
      type: "SET_AUTHEN",
      payload: {
        isAuth: true,
        user: {
          _id: "admin",
          fullname: "admin",
          username: "admin"
        }
      }
    })
  }, [dispatchAuth])

  const logout = useCallback(() => {
    dispatchAuth({
      type: "SET_AUTH",
      payload: {
        isAuth: false
      }
    })
  }, [dispatchAuth])

  const gridClick = useCallback(() => {
    return
  }, [])

  return (
    <div>
      <p>Home</p>
      <div>
        <Link to="/sign-in">SignIn</Link>
        <Link to="/sign-up">SignUp</Link>
        <button onClick={isAuth ? logout : login}>
          {isAuth ? "logout" : "login"}
        </button>
        {isAuth && user && (
          <>
            <GridComponent onClick={gridClick} />
            <p> {user.username} </p>
          </>
        )}
      </div>
    </div>
  )
}

export default Home
