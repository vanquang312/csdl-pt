import React from "react"
import { Link } from "react-router-dom"
import styles from "./style.module.scss"

const Home = () => {
  return (
    <div className={styles.container}>
      <p>SignUp</p>
      <Link to="/">Home</Link>
      <Link to="/sign-in">SignIn</Link>
    </div>
  )
}

export default Home
