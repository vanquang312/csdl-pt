import { GridComponent } from "@components/grid"
import { useAuth } from "@utils/index"
import { Col, Row, Table } from "antd"
import React, { forwardRef, useCallback, useEffect, useImperativeHandle, useState } from "react"
import { Link } from "react-router-dom"
import { range } from "lodash-es"
import { axiosClient } from "@/config"
import { ColumnsType } from "antd/es/table"
// import styles from "./index.module.css";

export interface PropsTable {
    onClickPatient: (_id) => void
}

export interface PropsTableRef {
    cancelSelect: () => void
    reload: () => void
}

const TableAdmision = forwardRef<PropsTableRef, PropsTable>(
    ({ onClickPatient }, ref) => {
        const [dataSource, setDataSource] = useState([])
        const [rowSelect, setRowSelect] = useState(null)

        useEffect(() => {
            getData()
        }, [])

        useImperativeHandle(ref, () => ({
            cancelSelect: () => setRowSelect(null),
            reload: getData
        }))

        const getData = () => {
            axiosClient.get("/admission").then(d => {
                console.log(d?.data)
                setDataSource(d?.data || [])
            })
        }

        const onSelectRow = value => {
            console.log(value)
            setRowSelect(value?._id)
            onClickPatient(value?._id)
        }

        const columns: ColumnsType<{}> = [
            {
                title: "Mã TN",
                dataIndex: "code",
                key: "code"
            },
            {
                title: "Mã BN",
                dataIndex: "patient",
                key: "code",
                render: value => value?.code
            },
            {
                title: "Tên bệnh nhân",
                dataIndex: "patient",
                key: "name",
                render: value => value?.name
            },
            {
                title: "Năm sinh",
                dataIndex: "boy",
                key: "boy"
            }
        ]

        console.log(dataSource)

        return (
            <Table
                dataSource={dataSource}
                columns={columns}
                pagination={false}
                scroll={{ y: "calc(100vh - 200px)" }}
                rowKey={(value: any) => value?._id}
                onRow={(record, rowIndex) => {
                    return {
                        onClick: () => onSelectRow(record),
                        style: { cursor: "pointer" },
                    }
                }}
                rowSelection={{
                    type: "radio",
                    onSelect: onSelectRow,
                    selectedRowKeys: rowSelect ? [rowSelect] : []
                }}
            />
        )
    }
)

export default TableAdmision
