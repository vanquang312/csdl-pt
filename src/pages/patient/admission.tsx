import {
    CloseOutlined,
    DeleteOutlined,
    EditOutlined,
    PlusCircleOutlined,
    SaveOutlined
} from "@ant-design/icons"
import { axiosClient } from "@/config"
import {
    Button,
    Col,
    DatePicker,
    Form,
    Input,
    message,
    notification,
    Popconfirm,
    Row,
    Select,
    Table
} from "antd"
import { useForm } from "antd/lib/form/Form"
import moment from "moment"
import React, {
    forwardRef,
    useCallback,
    useEffect,
    useImperativeHandle,
    useState
} from "react"
// import styles from "./index.module.css";

const { Option } = Select

export interface FormAdmissionRef {
    setFormValue: (value) => void
}

export interface Props {
    onCancel: () => void
    reload: () => void
}

const FormAdmission = forwardRef<FormAdmissionRef, Props>(({
    onCancel,
    reload
}, ref) => {
    const [form] = useForm()

    const [nn, setNN] = useState([])
    const [isEdit, setIsEdit] = useState<boolean>(false)
    const [quocGia, setQuocGia] = useState([])
    const [admission, setAdmission] = useState<any>(null)

    useEffect(() => {
        getNgheNghiep()
        getQuocGia()
    }, [])

    const getNgheNghiep = () => {
        axiosClient.get("/nghe-nghiep").then(d => {
            console.log(d?.data)
            setNN(d?.data || [])
        })
    }

    const getQuocGia = () => {
        axiosClient.get("/quoc-gia").then(d => {
            console.log(d?.data)
            setQuocGia(d?.data || [])
        })
    }

    useImperativeHandle(ref, () => ({
        setFormValue
    }))

    const setFormValue = value => {
        setAdmission(value),
            form?.setFieldsValue({
                ...value,
                bod: moment(value?.bod),
                name: value?.patient?.name,
                ID: value?.patient?.ID,
                sex: value?.patient?.sex,
                contry: value?.contry?._id,
                job: value?.job?._id,
            })
    }

    const onFinish = values => {
        axiosClient
            .post(`/admission/${admission ? `edit/${admission?._id}` : 'add'}`, {
                patient: {
                    name: values?.name,
                    ID: values?.ID,
                    bod: values?.bod?.valueOf(),
                    boy: values?.boy,
                    age: values?.age,
                    nation: values?.nation,
                    sex: values?.sex,
                    contry: values?.contry,
                    phoneNumber: values?.phoneNumber,
                    job: values?.job,
                    address: values?.address
                },
                service: values?.service,
                node: values?.node
            })
            .then(() => {
                notification.success({
                    message: "Lưu thành công",
                    placement: 'bottomLeft'
                })
                reload && reload()
                if (!admission) cancle()
            })
    }

    const onChangeBod = value => {
        console.log(value)
        if (!value) return
        console.log({
            boy: value?.year(),
            age: moment().diff(value, "year")
        })
        form?.setFieldsValue({
            boy: value?.year(),
            age: moment().diff(value, "year")
        })
    }

    const add = () => {
        setIsEdit(true)
        form?.resetFields()
        setAdmission(null)
        onCancel && onCancel()
    }

    const cancle = () => {
        setIsEdit(false)
        form?.resetFields()
        setAdmission(null)
        onCancel && onCancel()
    }

    const onRemove = () => {
        if (!admission) return
        axiosClient.delete(`/admission/remove/${admission?._id}`).then(() => {
            onCancel && onCancel()
            reload && reload()
            cancle()
        })
    }

    return (
        <Form
            name="form-admission"
            layout="vertical"
            onFinish={onFinish}
            form={form}
        >
            <Row gutter={[20, 0]}>
                <Col span={24}>
                    <Row justify="center" gutter={[10, 0]}>
                        <Col>
                            <Button
                                type="primary"
                                // disabled={admission ? false : true}
                                htmlType="submit"
                                icon={<SaveOutlined />}
                            >
                                Lưu
                            </Button>
                        </Col>
                        <Col>
                            <Button
                                type="primary"
                                onClick={cancle}
                                icon={<CloseOutlined />}
                            >
                                Hủy
                            </Button>
                        </Col>
                    </Row>
                </Col>
                <Col span={24}>
                    <h3>I. Thông tin hành chính</h3>
                </Col>
                <Col span={24}>
                    <Form.Item label="Mã BN" name="code">
                        <Input disabled />
                    </Form.Item>
                </Col>
                <Col span={16}>
                    <Form.Item label="Tên BN" name="name" rules={[{ required: true, message: 'Chưa nhập tên BN' }]}>
                        <Input />
                    </Form.Item>
                </Col>
                <Col span={8}>
                    <Form.Item label="CMND/CCCD" name="ID" rules={[{ required: true, message: 'Chưa nhập CMND/CCCD' }]}>
                        <Input />
                    </Form.Item>
                </Col>
                <Col span={6}>
                    <Form.Item label="Ngày sinh" name="bod" rules={[{ required: true, message: 'Chưa chọn ngày sinh' }]}>
                        <DatePicker
                            placeholder="Chọn ngày sinh"
                            format="DD/MM/YYYY"
                            onChange={onChangeBod}
                        />
                    </Form.Item>
                </Col>
                <Col span={6}>
                    <Form.Item label="Năm sinh" name="boy" rules={[{ required: true }]}>
                        <Input type="number" disabled />
                    </Form.Item>
                </Col>
                <Col span={24}>
                    <Form.Item label="Kết quả khám bệnh" name="result" rules={[{ required: true }]}>
                        <Input.TextArea  />
                    </Form.Item>
                </Col>
            </Row>
        </Form>
    )
})

export default FormAdmission
