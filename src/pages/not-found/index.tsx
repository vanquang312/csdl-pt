import React from "react"
import { Link } from "react-router-dom"

const Home = () => {
  return (
    <div>
      <p>404</p>
      <Link to="/">Home</Link>
    </div>
  )
}

export default Home
