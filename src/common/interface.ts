import { Dispatch } from "react"

export interface IRouter {
  exact: boolean
  path: string
  component: string
  noHeader?: boolean
  option?: any
  // permission?: TAppPermission,
  title?: string
}

// export interface IMenuRouter {
//     title: string,
//     type: "link" | "navigation" | "button",
//     dest: string,
//     childs?: {
//         title: string,
//         type: string,
//         dest: string,
//         icon?: string,
//         permission?: TAppPermission
//     }[],
//     permissions?: TAppPermission[]
// }

export interface IAuth {
  isAuth: boolean
  user?: IUserAuth | null
}

interface IDispathAuth {
  dispatchAuth: Dispatch<TAuthAction>
}

export interface TAuthContext extends IAuth, IDispathAuth {}

export interface IUserAuth {
  _id: string
  username: string
  fullname: string
}

export type TAuthAction =
  | {
      type: "SET_AUTH"
      payload: {
        isAuth: boolean
      }
    }
  | {
      type: "SET_USER"
      payload: {
        user?: IUserAuth
      }
    }
  | {
      type: "SET_AUTHEN"
      payload:
        | {
            isAuth: true
            user: IUserAuth
          }
        | {
            isAuth: false
          }
    }
