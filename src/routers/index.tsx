import { IRouter } from "@common/index"

const routersNotAuth: IRouter[] = [
  {
    exact: true,
    path: "/",
    component: "admission",
    noHeader: true
  },
  {
    exact: true,
    path: "/sign-in",
    component: "sign-in",
    noHeader: true
  },
  {
    exact: true,
    path: "/sign-up",
    component: "sign-up",
    noHeader: true
  },
  {
    exact: true,
    path: "/home/:id",
    component: "home",
    noHeader: true
  },
  {
    exact: true,
    path: "/kham-benh",
    component: "patient",
    noHeader: true
  }
]

export { routersNotAuth }
